#ifndef CALIBRATION_INTERFACE_H
#define CALIBRATION_INTERFACE_H
#pragma once

#include "data.h"
#include "io.h"
#include "optimization.h"
#include "pattern_operations.h"

#include <QObject>


namespace Calibration{

class Calibration_Interface : public QObject
{
    Q_OBJECT
public:

    ///
    /// \brief Calibration_Interface
    /// \param parent
    ///
    explicit Calibration_Interface(QObject *parent = nullptr);

    ///
    /// \brief init sets the data pointer
    /// \param data     pointer to the data class
    /// \return         true if successful, false otherwise
    ///
    bool init(Data* data);

public slots:

    ///
    /// \brief createPattern creates a calibration pattern according to the configuration saved in data_
    ///
    void createPattern();

    ///
    /// \brief savePattern saves the current pattern configuration to a user-specified dir
    ///
    void savePattern();

    ///
    /// \brief loadPattern loads a previously saved configuration from a dir selected by the user
    ///
    void loadPattern();

    ///
    /// \brief detectPatterns searches for and saves patterns in a list of images
    ///
    void detectPatterns();

    ///
    /// \brief loadCorrespondences asks the user for a previously saved correspondences file and loads its data
    /// \return
    ///
    bool loadCorrespondences();

    ///
    /// \brief calculateIntrinsics asks the user for a previously saved intrinsics file and loads its data
    /// \return
    ///
    bool calculateIntrinsics();

    ///
    /// \brief loadIntrinsics asks the user for a previously saved intrinsics file and loads its data
    /// \return
    ///
    bool loadIntrinsics();

    ///
    /// \brief calibrate checks the calibration requirements and starts the calibration process
    ///
    void calibrate();

    ///
    /// \brief saveCameras
    /// \return
    ///
    bool saveCameras();

    ///
    /// \brief loadCameras
    /// \return
    ///
    bool loadCameras();

    ///
    /// \brief calculatePatternPositions
    /// \return
    ///
    bool calculatePatternPositions();

    ///
    /// \brief loadPatternPositions
    /// \return
    ///
    bool loadPatternPositions();

    ///
    /// \brief savePatternPositions
    /// \return
    ///
    bool savePatternPositions();

private:

    ///contains calibration configuration data
    Data* data_;
    ///handles load and save operations for the configurations as well as results
    IO io_;
    ///handles pattern creation and detection
    Pattern_Operations pattern_ops_;
    ///handles the optimization functionality
    Optimization optimizer_;

signals:

    ///
    /// \brief updateGUI
    ///
    void updateGUI();

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(QString msg, int type);

    ///
    /// \brief logMat is a signal for the logger to log a matrix
    /// \param msg      message to display
    /// \param mat      matrix to display
    ///
    void logMat(QString msg, cv::Mat mat);
};
}
#endif // CALIBRATION_INTERFACE_H
