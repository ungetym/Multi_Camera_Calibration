#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H
#pragma once

#include "calibration_interface.h"
#include "calibration_widget.h"

#include <QMainWindow>

namespace Ui {
class Main_Window;
}

class Main_Window : public QMainWindow
{
    Q_OBJECT

public:

    explicit Main_Window(QWidget *parent = 0);
    ~Main_Window();

private:

    Ui::Main_Window *ui_;
    Calibration::Calibration_Interface calibration_interface_;
    Calibration::Data data_;
};

#endif // MAIN_WINDOW_H
