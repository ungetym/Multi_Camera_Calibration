#ifndef PATTERN_OPERATIONS_H
#define PATTERN_OPERATIONS_H
#pragma once

#include "data.h"

#include <QObject>


namespace Calibration{

class Pattern_Operations : public QObject
{
    Q_OBJECT

public:

    ///
    /// \brief Pattern_Operations is the standard constructor
    /// \param parent
    ///
    explicit Pattern_Operations(QObject *parent = nullptr);

    ///
    /// \brief createChArUcoPattern creates a ChArUco calibration pattern
    /// \param width            number of horizontal checkerboard squares
    /// \param height           number of vertical checkerboard squares
    /// \param charuco_params   struct containing the charuco specific parameters
    /// \param pattern          pointer to image the generated pattern is saved to
    /// \return                 true if pattern generated successfully, false otherwise
    ///
    bool createChArUcoPattern(const int width, const int height, const ChArUco_Params& charuco_params, cv::Mat* pattern);

    ///
    /// \brief createRandomPattern creates a random dot pattern
    /// \param width            horizontal size
    /// \param height           vertical size
    /// \param pattern          pointer to image the generated pattern is saved to
    /// \return                 true if pattern generated successfully, false otherwise
    ///
    bool createRandomPattern(const int width, const int height, cv::Mat* pattern);

    ///
    /// \brief detectChArUcoPatterns detects charuco patterns in the given files
    /// \param width            number of horizontal checkerboard squares
    /// \param height           number of vertical checkerboard squares
    /// \param charuco_params   contains the charuco specific parameters
    /// \param files            contains a list of the image files
    /// \param detection        contains the output vectors for the detected patterns
    /// \return                 true if detection was successful and without errors
    ///
    bool detectChArUcoPatterns(const int width, const int height, const ChArUco_Params& charuco_params, const Files& files, Detection* detection);

    ///
    /// \brief detectRandomPatterns
    /// \param width            horizontal size
    /// \param height           vertical size
    /// \param pattern          image of the random pattern
    /// \param files            contains a list of the image files
    /// \param detection        contains the output vectors for the detected patterns
    /// \return                 true if detection was successful and without errors
    ///
    bool detectRandomPatterns(const int width, const int height, const cv::Mat& pattern, const Files& files, Detection* detection);

private:

    ///
    /// \brief convertFeaturelistsToMat converts the lists of detected correspondences to lists cv::Mat with each cv::Mat containing the detected points for one input image
    /// \param detection        detected correspondeces
    /// \return                 true if conversion successful, false otherwise
    ///
    bool convertFeaturelistsToMat(Detection* detection);

signals:

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(QString msg, int type);
};
}
#endif // PATTERN_OPERATIONS_H
