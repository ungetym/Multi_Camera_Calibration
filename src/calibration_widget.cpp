#include "calibration_widget.h"
#include "ui_calibration_widget.h"

#include <QDesktopServices>
#include <QFileDialog>

using namespace Calibration;

Calibration_Widget::Calibration_Widget(QWidget *parent) :
    QWidget(parent),
    ui_(new Ui::Calibration_Widget)
{
    ui_->setupUi(this);
    this->setObjectName("Calibration Widget");
}

Calibration_Widget::~Calibration_Widget(){
    delete ui_;
}

bool Calibration_Widget::init(Data *data){
    if(data == nullptr){
        ERROR("Calibration widget initialization failed. Data pointer is NULL.");
        return false;
    }

    data_ = data;

    //connect data class signals
    connect(data_, &Calibration::Data::updateGUI, this, &Calibration_Widget::updateGUI);

    //connect pattern configuration GUI and data class
    connect(ui_->comboBox_calibration_pattern_type, qOverload<int>(&QComboBox::currentIndexChanged), this, &Calibration_Widget::setParameter);
    connect(ui_->lineEdit_pattern_width,&QLineEdit::textEdited, this, &Calibration_Widget::setParameter);
    connect(ui_->lineEdit_pattern_height,&QLineEdit::textEdited, this, &Calibration_Widget::setParameter);
    connect(ui_->lineEdit_real_square_size,&QLineEdit::textEdited, this, &Calibration_Widget::setParameter);
    connect(ui_->slider_border_size, &QSlider::valueChanged, this, &Calibration_Widget::setParameter);
    connect(ui_->comboBox_dictionary_size, qOverload<int>(&QComboBox::currentIndexChanged), this, &Calibration_Widget::setParameter);
    connect(ui_->comboBox_marker_size, qOverload<int>(&QComboBox::currentIndexChanged), this, &Calibration_Widget::setParameter);
    connect(ui_->lineEdit_marker_border_size,&QLineEdit::textEdited, this, &Calibration_Widget::setParameter);
    connect(ui_->slider_marker_ratio, &QSlider::valueChanged, this, &Calibration_Widget::setParameter);

    //connect pattern detection elements
    connect(ui_->button_select_input_dir, &QPushButton::clicked, this, &Calibration_Widget::selectDir);
    connect(ui_->button_open_input_dir, &QPushButton::clicked, this, &Calibration_Widget::openDir);
    connect(ui_->checkBox_use_rig_position_file, &QCheckBox::clicked, [this](bool checked){data_->use_rig_position_file = checked;});

    //connect intrinsics elements
    connect(ui_->comboBox_initial_intrinsics, qOverload<int>(&QComboBox::currentIndexChanged), this, &Calibration_Widget::setParameter);

    //connect optimization elements
    connect(ui_->lineEdit_LM_max_iterations,&QLineEdit::textEdited, this, &Calibration_Widget::setParameter);
    connect(ui_->lineEdit_LM_change_threshold,&QLineEdit::textEdited, this, &Calibration_Widget::setParameter);
    connect(ui_->comboBox_LM_termination_criterium, qOverload<int>(&QComboBox::currentIndexChanged), this, &Calibration_Widget::setParameter);
    connect(ui_->comboBox_refine_intrinsics, qOverload<int>(&QComboBox::currentIndexChanged), this, &Calibration_Widget::setParameter);
    connect(ui_->comboBox_optimize_extrinsics, qOverload<int>(&QComboBox::currentIndexChanged), this, &Calibration_Widget::setParameter);
    connect(ui_->comboBox_initial_extrinsics, qOverload<int>(&QComboBox::currentIndexChanged), this, &Calibration_Widget::setParameter);
    connect(ui_->checkBox_refine_rig_axes, &QCheckBox::clicked, [this](bool checked){data_->opt_params.refine_rig = checked;});

    updateGUI();

    return true;
}

void Calibration_Widget::selectDir(){
    //open file dialog
    QString dir_name_q = QFileDialog::getExistingDirectory(this,"Choose a directory","~",QFileDialog::ShowDirsOnly);

    std::string dir_name = dir_name_q.toStdString();
    if(dir_name.size()==0){
        STATUS("No directory chosen.");
        return;
    }

    data_->input_dir = dir_name;
    ui_->lineEdit_image_input_dir->setText(dir_name_q);
    STATUS("Calibration image dir set to"+dir_name_q);
}

void Calibration_Widget::openDir(){
    if(data_->input_dir.size()==0){
        ERROR("No directory set.");
        return;
    }
    else{
        //check if dir exists
        QString dir_name_q = QString::fromStdString(data_->input_dir);
        QDir dir(dir_name_q);
        if(!dir.exists()){
            ERROR("Directory does not exist.");
            return;
        }
        else{
            //open dir in default file explorer
            QDesktopServices::openUrl(QUrl::fromLocalFile(dir_name_q));
        }
    }
}

void Calibration_Widget::setParameter(QVariant value){
    QObject* sender = QObject::sender();
    if(sender->objectName() == "comboBox_calibration_pattern_type"){
        data_->pattern_type = value.toInt();
        updateGUI();
    }
    else if(sender->objectName() == "slider_border_size"){
        data_->charuco_params.border_size = (float)value.toInt() / 100.0;
        ui_->display_border_ratio->setText(QString::number(value.toInt()));
    }
    else if(sender->objectName() == "comboBox_dictionary_size"){
        data_->charuco_params.dict = ARUCO_CODES[ui_->comboBox_marker_size->currentIndex()*4+value.toInt()];
    }
    else if(sender->objectName() == "comboBox_marker_size"){
        data_->charuco_params.dict = ARUCO_CODES[value.toInt()*4+ui_->comboBox_dictionary_size->currentIndex()];
    }
    else if(sender->objectName() == "slider_marker_ratio"){
        data_->charuco_params.marker_to_square_ratio = (float)value.toInt() / 100.0;
        ui_->display_marker_ratio->setText(QString::number(value.toInt()));
    }
    else if(sender->objectName() == "comboBox_LM_termination_criterium"){
        data_->lm_params.term_criterium = value.toInt();
    }
    else if(sender == ui_->comboBox_initial_intrinsics){
        data_->opt_params.initial_intrinsics_mode = value.toInt();
    }
    else if(sender == ui_->comboBox_refine_intrinsics){
        data_->opt_params.refine_intrinsics_mode = value.toInt();
    }
    else if(sender == ui_->comboBox_optimize_extrinsics){
        data_->opt_params.calculate_extrinsics_mode = value.toInt();
    }
    else if(sender == ui_->comboBox_initial_extrinsics){
        data_->opt_params.initial_extrinsics_mode = value.toInt();
    }
    else{
        if(sender->objectName().contains("lineEdit")){
            if(value.toString().size() == 0){
                return;
            }

            if(sender->objectName() == "lineEdit_pattern_width"){
                if(!data_->setParameter(value.toString(),PARAM_PATTERN_WIDTH)){
                    updateGUI();
                }
            }
            else if(sender->objectName() == "lineEdit_pattern_height"){
                if(!data_->setParameter(value.toString(),PARAM_PATTERN_HEIGHT)){
                    updateGUI();
                }
            }
            else if(sender->objectName() == "lineEdit_real_square_size"){
                if(!data_->setParameter(value.toString(),PARAM_CHARUCO_SQUARE_SIZE)){
                    updateGUI();
                }
            }
            else if(sender->objectName() == "lineEdit_marker_border_size"){
                if(!data_->setParameter(value.toString(),PARAM_CHARUCO_BORDER_PIXEL)){
                    updateGUI();
                }
            }
            else if(sender->objectName() == "lineEdit_LM_max_iterations"){
                if(!data_->setParameter(value.toString(),PARAM_LM_MAX_ITERATIONS)){
                    updateGUI();
                }
            }
            else if(sender->objectName() == "lineEdit_LM_change_threshold"){
                if(!data_->setParameter(value.toString(),PARAM_LM_CHANGE_THRESHOLD)){
                    updateGUI();
                }
            }
        }
    }
}

void Calibration_Widget::updateGUI(){
    //update pattern configuration
    ui_->groupBox_pattern_configuration->setEnabled(false);
    ui_->comboBox_calibration_pattern_type->setCurrentIndex(data_->pattern_type);

    if(data_->pattern_type == CALIB_PATTERN_CHARUCO){
        ui_->lineEdit_pattern_width->setText(QString::number(data_->patternWidth()));
        ui_->lineEdit_pattern_height->setText(QString::number(data_->patternHeight()));
        ui_->lineEdit_real_square_size->setText(QString::number(data_->charuco_params.square_size));
        ui_->slider_border_size->setValue(std::min(100,(int)(100.0*data_->charuco_params.border_size)));
        ui_->display_border_ratio->setText(QString::number(std::min(100,(int)(100.0*data_->charuco_params.border_size+0.5))));
        int index = std::distance(ARUCO_CODES.begin(), std::find(ARUCO_CODES.begin(), ARUCO_CODES.end(), data_->charuco_params.dict));
        ui_->comboBox_dictionary_size->setCurrentIndex(index % 4);
        ui_->comboBox_marker_size->setCurrentIndex((int)((float)index/4.0));
        ui_->lineEdit_marker_border_size->setText(QString::number(data_->charuco_params.marker_border_pixel));
        ui_->slider_marker_ratio->setValue(std::min(100,(int)(100.0*data_->charuco_params.marker_to_square_ratio+0.5)));
        ui_->display_marker_ratio->setText(QString::number(std::min(100,(int)(100.0*data_->charuco_params.marker_to_square_ratio))));
        //activate charuco related GUI options
        ui_->frame_charuco_parameters->setEnabled(true);
    }
    else{
        //deactivate charuco related GUI options
        ui_->frame_charuco_parameters->setEnabled(false);
    }

    ui_->groupBox_pattern_configuration->setEnabled(true);

    //update calibration configuration
    ui_->groupBox_correspondences->setEnabled(false);
    ui_->groupBox_intrinsics->setEnabled(false);
    ui_->groupBox_optimization->setEnabled(false);

    ui_->lineEdit_image_input_dir->setText(QString::fromStdString(data_->input_dir));
    ui_->checkBox_use_rig_position_file->setChecked(data_->use_rig_position_file);

    ui_->comboBox_initial_intrinsics->setCurrentIndex(data_->opt_params.initial_intrinsics_mode);

    ui_->lineEdit_LM_max_iterations->setText(QString::number(data_->lm_params.max_iterations));
    ui_->lineEdit_LM_change_threshold->setText(QString::number(data_->lm_params.change_threshold));
    ui_->comboBox_LM_termination_criterium->setCurrentIndex(data_->lm_params.term_criterium);
    ui_->comboBox_refine_intrinsics->setCurrentIndex(data_->opt_params.refine_intrinsics_mode);
    ui_->comboBox_optimize_extrinsics->setCurrentIndex(data_->opt_params.calculate_extrinsics_mode);
    ui_->comboBox_initial_extrinsics->setCurrentIndex(data_->opt_params.initial_extrinsics_mode);
    ui_->checkBox_refine_rig_axes->setChecked(data_->opt_params.refine_rig);

    ui_->groupBox_correspondences->setEnabled(true);
    ui_->groupBox_intrinsics->setEnabled(true);
    ui_->groupBox_optimization->setEnabled(true);
}
