#ifndef CALIBRATION_WIDGET_H
#define CALIBRATION_WIDGET_H
#pragma once

#include "data.h"

#include <QVariant>
#include <QWidget>

namespace Ui {
class Calibration_Widget;
}

class Calibration_Widget : public QWidget
{
    Q_OBJECT

public:

    ///
    /// \brief Calibration_Widget is the standard constructor
    /// \param parent       widget containing this calibration widget
    ///
    explicit Calibration_Widget(QWidget* parent = nullptr);
    ///destructor
    ~Calibration_Widget();

    ///
    /// \brief init sets the data_ pointer and connects GUI to data
    /// \param data         pointer to data class
    /// \return             true if successful, false otherwise
    ///
    bool init(Calibration::Data* data);

    ///
    /// \brief getUI is a getter for ui_
    /// \return             pointer to gui elements
    ///
    Ui::Calibration_Widget* getUI(){return ui_;}

public slots:

    ///
    /// \brief selectDir opens a dialog for the user to select a directory
    ///
    void selectDir();

    ///
    /// \brief openDir opens the default file browser at the location specified in data_
    ///
    void openDir();

    ///
    /// \brief updateGUI sets the parameters given by data_ to the GUI
    ///
    void updateGUI();

private:

    ///GUI elements
    Ui::Calibration_Widget* ui_;
    ///contains calibration configuration data
    Calibration::Data* data_;

private slots:
    ///
    /// \brief setParameter handles parameter setting for slider, comboboxes and lineedits
    /// \param value    input parameter given by the corresponding signal
    ///
    void setParameter(QVariant value = (QString)"");

signals:

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(QString msg, int type);
};

#endif // CALIBRATION_WIDGET_H
