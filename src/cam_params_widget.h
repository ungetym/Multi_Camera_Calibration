#ifndef CAM_PARAMS_WIDGET_H
#define CAM_PARAMS_WIDGET_H
#pragma once

#include "data.h"

#include <QWidget>

namespace Ui {
class Cam_Params_Widget;
}

class Cam_Params_Widget : public QWidget
{
    Q_OBJECT

public:

    ///
    /// \brief Cam_Params_Widget is the standard constructor
    /// \param parent
    ///
    explicit Cam_Params_Widget(QWidget *parent = 0);

    ///destructor
    ~Cam_Params_Widget();

    ///
    /// \brief init
    /// \param estimation
    /// \return
    ///
    bool init(Calibration::Estimation* estimation);

public slots:

    ///
    /// \brief updateGUI
    ///
    void updateGUI();

private:

    Ui::Cam_Params_Widget* ui_;
    Calibration::Estimation* estimation_;

private slots:

    ///
    /// \brief setActiveParameters
    /// \param idx
    ///
    void setActiveParameters(int idx);

    ///
    /// \brief updateCameraParameters
    ///
    void updateCameraParameters();


signals:

    ///
    /// \brief loadCams
    ///
    void loadCams();

    ///
    /// \brief saveCams
    ///
    void saveCams();

};

#endif // CAM_PARAMS_WIDGET_H
