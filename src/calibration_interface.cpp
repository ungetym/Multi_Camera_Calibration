#include "calibration_interface.h"
#include "helper.h"

#include <QFileDialog>
#include <QThread>

using namespace Calibration;

Calibration_Interface::Calibration_Interface(QObject *parent) : QObject(parent){
    this->setObjectName("Calibration Interface");

    //connect member logger signals
    connect(&io_, &IO::log, this, &Calibration_Interface::log);
    connect(&pattern_ops_, &Pattern_Operations::log, this, &Calibration_Interface::log);
    connect(&optimizer_, &Optimization::log, this, &Calibration_Interface::log);
    connect(&optimizer_, &Optimization::logMat, this, &Calibration_Interface::logMat);

}

bool Calibration_Interface::init(Data* data){
    bool success = true;

    if(data == nullptr){
        ERROR("Initialization failed. Data pointer is invalid.");
        success = false;
    }

    if(success){
        data_ = data;
    }

    return success;
}

void Calibration_Interface::createPattern(){
    bool success = true;
    cv::Mat pattern;
    if(data_->pattern_type == CALIB_PATTERN_CHARUCO){
        success = pattern_ops_.createChArUcoPattern(data_->patternWidth(), data_->patternHeight(), data_->charuco_params, &pattern);
    }
    else if(data_->pattern_type == CALIB_PATTERN_RANDOM){
        success = pattern_ops_.createRandomPattern(data_->patternWidth(), data_->patternHeight(), &pattern);
    }

    if(success){
        data_->setPatternImage(&pattern);
    }
}

void Calibration_Interface::savePattern(){
    //ask user for file output location and name
    QString file_path_q = QFileDialog::getSaveFileName(nullptr, tr("Save File"),"./",tr("Calibration pattern configuration (*.MCC_Patt)"));
    if(file_path_q.size() == 0){
        file_path_q = "pattern.MCC_Patt";
    }
    std::string file_path = file_path_q.toStdString();
    //check if file_name ends with .MCC_Patt
    if(file_path.size()>9){
        if(file_path.substr(file_path.size()-9,9).compare(".MCC_Patt")!=0){
            file_path+=".MCC_Patt";
        }
    }
    else{
        file_path+=".MCC_Patt";
    }

    //save config
    if(!io_.saveParameters(file_path, data_->meta_calib_param_list)){
        ERROR("Calibration pattern parameters could not be saved.");
        return;
    }

    //save image
    if(!cv::imwrite(file_path+".bmp", data_->pattern_image)){
        ERROR("Calibration pattern image could not be saved.");
        return;
    }
}

void Calibration_Interface::loadPattern(){
    //ask user for pattern file dir location
    QString file_path_q = QFileDialog::getOpenFileName(nullptr, tr("Load File"),"./",tr("Calibration pattern configuration (*.MCC_Patt)"));
    std::string file_path = file_path_q.toStdString();
    if(file_path.size()==0){
        STATUS("No directory chosen.");
        return;
    }

    //load config
    if(!io_.loadParameters(file_path, &data_->meta_calib_param_list)){
        ERROR("Calibration pattern could not be loaded.");
        return;
    }

    //set GUI to loaded calibration pattern type
    emit updateGUI();

    //load image
    cv::Mat pattern = cv::imread(file_path+".bmp",cv::IMREAD_GRAYSCALE);
    data_->setPatternImage(&pattern);
}

void Calibration_Interface::detectPatterns(){
    STATUS("Start pattern detection for input images..");

    //check if image input dir is set
    if(data_->input_dir.size() == 0){
        ERROR("Input image dir not set.");
        return;
    }

    //load rig position file if checked
    std::map<int,pairf> rig_positions;
    if(data_->use_rig_position_file){
        //try to automatically load the file
        std::string file_path = data_->input_dir+"/rig_positions.txt";
        STATUS("Try auto load of rig position file.");
        if(!io_.loadRigPositions(file_path,&data_->detection.num_rig_positions, &rig_positions)){
            STATUS("Use user input for rig position file.");
            //ask user for file location
            QString file_path_q = QFileDialog::getOpenFileName(nullptr, tr("Load File"), QString::fromStdString(data_->input_dir), tr("Rig position file (*.txt)"));
            file_path = file_path_q.toStdString();
            bool failed = false;
            if(file_path.size()==0){
                STATUS("No directory chosen.");
                failed = true;
            }
            else{
                failed = !io_.loadRigPositions(file_path,&data_->detection.num_rig_positions, &rig_positions);
            }

            if(failed){
                //ask user to calibrate without rig position file
                if(!Helper::dialogOkCancel("Do you want to continue without rig position input?")){
                    return;
                }
                else{
                    data_->detection.num_rig_positions = 1;
                }
            }

        }
    }
    else{
        data_->detection.num_rig_positions = 1;
        data_->detection.rig_positions.clear();
        data_->detection.rig_positions.push_back(pairf(0.0,0.0));
    }

    //create image list and initial cameras
    std::vector<Camera> cameras;
    if(!io_.createImageList(data_->input_dir, rig_positions, &data_->files, &data_->detection, &cameras)){
        ERROR("Image list creation failed.");
        return;
    }
    data_->estimation.setCameras(cameras ,SET_CAMERA_ALL, false);

    //detect pattern
    if(data_->pattern_type == CALIB_PATTERN_CHARUCO){
        if(!pattern_ops_.detectChArUcoPatterns(data_->patternWidth(),data_->patternHeight(),data_->charuco_params,data_->files, &data_->detection)){
            ERROR("Unable to detect ChArUco patterns in images.");
            return;
        }
    }
    else if(data_->pattern_type == CALIB_PATTERN_RANDOM){
        if(!pattern_ops_.detectRandomPatterns(data_->patternWidth(),data_->patternHeight(),data_->pattern_image, data_->files, &data_->detection)){
            ERROR("Unable to detect random patterns in images.");
            return;
        }
    }


    //ask user to save patterns
    if(Helper::dialogYesNo("Do you want to save the detected pattern points?")){
        //ask user for file output location and name
        QString file_name_q = QFileDialog::getSaveFileName(nullptr, tr("Save correspondences to file"),"./",tr("Corner files (*.MCC_Corr)"));
        if(file_name_q.size() == 0){
            file_name_q = "correspondences.MCC_Corr";
        }
        std::string file_path = file_name_q.toStdString();
        //check if file_name ends with .MCC_Corr
        if(file_path.size()>9){
            if(file_path.substr(file_path.size()-9,9).compare(".MCC_Corr")!=0){
                file_path+=".MCC_Corr";
            }
        }
        else{
            file_path+=".MCC_Corr";
        }

        if(!io_.saveCorrespondences(file_path,data_->detection.correspondences,data_->detection.rig_positions, data_->estimation.cams)){
            ERROR("Saving of correspondences failed!");
        }
    }

    STATUS("Pattern detection complete.");
}

bool Calibration_Interface::loadCorrespondences(){
    QString file_name_q = QFileDialog::getOpenFileName(nullptr, tr("Load File"),"./",tr("Correspondences files (*.MCC_Corr)"));
    if(file_name_q.size()==0){
        STATUS("No correspondeces file chosen.");
        return false;
    }
    std::string file_path = file_name_q.toStdString();

    std::vector<Camera> cameras;
    if(!io_.loadCorrespondences(file_path,&data_->detection, &data_->estimation)){
        ERROR("Loading correspondences failed.");
        return false;
    }
    data_->estimation.setCameras(cameras, SET_CAMERA_ALL, false);

    STATUS("Correspondences successfully loaded.");
    return true;
}

bool Calibration_Interface::calculateIntrinsics(){
    STATUS("Start intrinsics estimation. Please wait..");
    data_->estimation.clearGeometry();

    //load or detect correspondences if not available
    if(data_->detection.correspondences.size() == 0){
        if(Helper::askUser("No correspondences available. Do you want to calculate or load the correspondences?", "Calculate", "Load") == 0){
            detectPatterns();
        }
        else{
            loadCorrespondences();
        }
    }

    if(data_->detection.correspondences.size() == 0){
        ERROR("No correspondences available - either load them from a file or detect the patterns in the input images.");
        return false;
    }


    if(data_->opt_params.initial_intrinsics_mode == INIT_INTRINSICS_FILE){
        STATUS("Load initial intrinsics from file..");
        QString file_name_q = QFileDialog::getOpenFileName(nullptr, tr("Load File"),"./",tr("Intrinsics files (*.MCC_Intr)"));
        if(file_name_q.size()==0){
            STATUS("No intrinsics file chosen.");
        }
        else{
            std::string file_path = file_name_q.toStdString();
            std::vector<Camera> cameras;
            if(!io_.loadCameras(file_path,&cameras)){
                WARN("Loading intrinsics failed. Proceed without initial intrinsics");
                data_->opt_params.initial_intrinsics_mode = INIT_INTRINSICS_NONE;
                data_->updateGUI();
            }
            else{
                STATUS("Intrinsics successfully loaded.");
                data_->estimation.setCameras(cameras);
            }
        }
    }

    bool calc_transforms_only = true;
    for(const Camera& cam : data_->estimation.cams){
        if(cam.calculate_intrinsics){
            calc_transforms_only = false;
            break;
        }
    }


    if(!optimizer_.intrinsicsEstimation(data_->detection,&data_->estimation, data_->opt_params.initial_intrinsics_mode)){
        if(calc_transforms_only){
            ERROR("Transformation calculation failed.");
        }
        else{
            ERROR("Intrinsic calibration failed.");
        }
        return false;
    }
    if(calc_transforms_only){
        STATUS("Transformation successfully calculated.");
    }
    else{
        STATUS("Intrinsics successfully calculated.");
    }

    //ask user to save intrinsics and estimated rotations/translations
    if(!calc_transforms_only && Helper::dialogYesNo("Do you want to save the calculated intrinsics?")){
        if(!saveCameras()){
            WARN("Saving the intrinsics failed.");
        }
    }
    if(Helper::dialogYesNo("Do you want to save the calculated transformations?")){
        if(!savePatternPositions()){
            WARN("Saving transformations failed.");
        }
    }

    return true;
}

bool Calibration_Interface::loadIntrinsics(){
    data_->estimation.clearGeometry();

    QString file_name_q = QFileDialog::getOpenFileName(nullptr, tr("Load File"),"./",tr("Intrinsics files (*.MCC_Cams)"));
    if(file_name_q.size()==0){
        STATUS("No intrinsics file chosen.");
        return false;
    }
    std::string file_path = file_name_q.toStdString();

    std::vector<Camera> cameras;
    if(!io_.loadCameras(file_path,&cameras)){
        ERROR("Loading intrinsics failed.");
        return false;
    }
    if(Helper::dialogYesNo("The camera parameter file also contains camera poses. Should these be loaded, too?")){
        data_->estimation.setCameras(cameras);
    }
    else{
        data_->estimation.setCameras(cameras, SET_CAMERA_INTRINSICS);
    }

    STATUS("Intrinsics successfully loaded.");
    return true;
}

void Calibration_Interface::calibrate(){
    //load or detect correspondences if not available
    if(data_->detection.correspondences.size() == 0){
        if(Helper::askUser("No correspondences available. Do you want to calculate or load the correspondences?", "Calculate", "Load") == 0){
            detectPatterns();
        }
        else{
            loadCorrespondences();
        }
    }

    if(data_->detection.correspondences.size() == 0){
        ERROR("No correspondences for calibration available - either load them from a file or detect the patterns in the input images.");
        return;
    }

    //check if more than one rig position is available
    if(data_->detection.num_rig_positions == 1 && data_->opt_params.refine_rig){
        WARN("Rig axes refinement is not available since input images were taken at the same rig position.");
        data_->opt_params.refine_rig = false;
        data_->updateGUI();
        QThread::msleep(1500);
    }

    //exemplarily check first camera for intrinsics
    bool intrinsics_caluclated = true;
    cv::Mat K = data_->estimation.cams[0].K;
    if(K.at<double>(0,0) == 1.0){
        int answer = Helper::askUser("The intrinsics have seemingly not been calculated. Do you want to load or calculate the intrinsics?", "No", "Yes, load intrinsics", "Yes, calculate intrinsics");
        if(answer == 1){
            if(!loadIntrinsics()){
                if(Helper::dialogYesNo("Loading the intrinsics failed. Do you want to recalculate the intrinsics?")){
                    if(!calculateIntrinsics()){
                        intrinsics_caluclated = false;
                    }
                }
                else{
                    intrinsics_caluclated = false;
                }
            }
        }
        else if(answer == 2){
            if(!calculateIntrinsics()){
                if(Helper::dialogYesNo("Calculating the intrinsics failed. Do you want to load the intrinsics?")){
                    if(!loadIntrinsics()){
                        intrinsics_caluclated = false;
                    }
                }
                else{
                    intrinsics_caluclated = false;
                }
            }
        }

        if(intrinsics_caluclated){
            if(Helper::askUser("Do you want to load or calculate the initial pattern positions?", "Load", "Calculate") == 0){
                if(!loadPatternPositions()){
                    intrinsics_caluclated = false;
                }
            }
            else{
                if(!calculatePatternPositions()){
                    intrinsics_caluclated = false;
                }
            }
            if(!intrinsics_caluclated){
                STATUS("The initial pattern positions have not been calculated or loaded. ");
                return;
            }
        }
        else{
            STATUS("The intrinsics have not been calculated or loaded. ");
            return;
        }

    }

    //start optimization
    if(!optimizer_.optimizeExtrinsics(data_->detection, &data_->estimation, data_->lm_params, data_->opt_params)){
        ERROR("Optimization of extrinsic parameters not successful.");
        return;
    }

    //ask user if estimated cameras should be saved for further operations
    if(data_->estimation.cams.size() > 0){
        if(Helper::dialogYesNo("Do you want to save the estimated camera parameters?")){
            saveCameras();
        }
        if(Helper::dialogYesNo("Do you want to save the estimated transformations between the cameras and patterns?")){
            savePatternPositions();
        }
    }
}

bool Calibration_Interface::saveCameras(){
    //ask user for file output location and name
    QString file_name_q = QFileDialog::getSaveFileName(nullptr, tr("Save cameras to file"),"./",tr("Camera parameter files (*.MCC_Cams)"));
    if(file_name_q.size() == 0){
        file_name_q = "cam_params.MCC_Cams";
    }
    std::string file_path = file_name_q.toStdString();
    //check if file_name ends with .MCC_Cams
    if(file_path.size()>9){
        if(file_path.substr(file_path.size()-9,9).compare(".MCC_Cams")!=0){
            file_path+=".MCC_Cams";
        }
    }
    else{
        file_path+=".MCC_Cams";
    }

    if(!io_.saveCameras(file_path,data_->estimation.cams)){
        ERROR("Saving cameras failed!");
        return false;
    }
    return true;
}

bool Calibration_Interface::loadCameras(){
    //ask user for file input location and name
    QString file_name_q = QFileDialog::getOpenFileName(nullptr, tr("Load file"),"./",tr("Camera parameter files (*.MCC_Cams)"));
    if(file_name_q.size()==0){
        STATUS("No parameter file chosen.");
        return false;
    }
    std::string file_path = file_name_q.toStdString();

    std::vector<Camera> cameras;
    if(!io_.loadCameras(file_path,&cameras)){
        ERROR("Loading cameras failed.");
        return false;
    }
    data_->estimation.setCameras(cameras);

    STATUS("Cameras successfully loaded.");
    return true;
}

bool Calibration_Interface::calculatePatternPositions(){
    data_->opt_params.initial_intrinsics_mode = INIT_INTRINSICS_NONE;
    data_->updateGUI();
    //temporarily set all cameras
    std::vector<bool> temp = data_->estimation.getCalcIntrinsics();
    if(!data_->estimation.setCalcIntrinsics(std::vector<bool>(data_->estimation.cams.size(),false))){
          return false;
    }
    bool success = calculateIntrinsics();
    if(!data_->estimation.setCalcIntrinsics(temp)){
        return false;
    }
    return success;
}

bool Calibration_Interface::loadPatternPositions(){
    //ask user for file input location and name
    QString file_name_q = QFileDialog::getOpenFileName(nullptr, tr("Load file"),"./",tr("Pattern position files (*.MCC_Trafo)"));
    if(file_name_q.size()==0){
        STATUS("No parameter pattern position file chosen.");
        return false;
    }
    std::string file_path = file_name_q.toStdString();

    if(!io_.loadTransformations(file_path,&data_->estimation)){
        ERROR("Loading transformations failed.");
        return false;
    }
    STATUS("Transformations successfully loaded.");
    return true;
}

bool Calibration_Interface::savePatternPositions(){
    //ask user for file output location and name
    QString file_name_q = QFileDialog::getSaveFileName(nullptr, tr("Save pattern positions to file"),"./",tr("Pattern position files (*.MCC_Trafo)"));
    if(file_name_q.size() == 0){
        file_name_q = "positions.MCC_Trafo";
    }
    std::string file_path = file_name_q.toStdString();
    //check if file_name ends with .MCC_Trafo
    if(file_path.size()>10){
        if(file_path.substr(file_path.size()-10,10).compare(".MCC_Trafo")!=0){
            file_path+=".MCC_Trafo";
        }
    }
    else{
        file_path+=".MCC_Trafo";
    }

    if(!io_.saveTransformations(file_path,data_->estimation)){
        ERROR("Saving pattern positions failed!");
        return false;
    }
    return true;
}
