# Multi Camera Calibration
Toolkit for the calibration of a system consisting of multiple cameras, possibly mounted onto a 2 axes rig as used in [our light field research](https://www.mip.informatik.uni-kiel.de/en/research/light-field-capturing). The calibration is based on ChArUco boards, a combination of the classical checkerboard and ArUco markers, which allows the calibration pattern to be partly occluded.

![GUI Overview](https://gitlab.com/ungetym/Multi_Camera_Calibration/raw/master/images/tutorial_basic_50.png "GUI Overview")

Requires:
- [QT 5](https://www.qt.io/download-qt-installer)
- OpenCV >=4 (for version 3 checkout commit dcc520f3)
- Compiler with C++14 support (e.g. gcc)

Build instructions:
- A CMakeLists.txt as well as a .pro file are provided in the source folder. (Thanks to M-A-Robson! :))
- For the first option, use the usual 'mkdir build', 'cd build', 'cmake ..', 'make' pipeline or open the CMakeLists.txt file via an IDE which supports cmake (e.g. QtCreator).
- For the second option, simply open the .pro file via QtCreator and modify it to set the correct OpenCV path. You should now be able to build the tool.

Some instructions on the usage of this tool are given in [this repository's Wiki](https://gitlab.com/ungetym/Multi_Camera_Calibration/wikis/Basic-Workflow).

For further information contact me via <tmi@informatik.uni-kiel.de>.
